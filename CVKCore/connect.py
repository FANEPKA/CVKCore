import importlib
import sys
import os
from asyncio.events import AbstractEventLoop
from vkbottle.bot import Bot
from .errors.BotError import *
from .utils.DataBaseConnect import MySQL
from vkbottle import ConsistentTokenGenerator

class BotConnect:

    db: MySQL = None
    modules = []

    def __init__(self, tokens: ConsistentTokenGenerator, loop: AbstractEventLoop):
        self.bot = Bot(ConsistentTokenGenerator(tokens), loop = loop)
        self.loop = loop

    def connect_to_db(self, host: str, user: str, password: str, db: str):
        self.db = MySQL(host, user, password, db, self.loop)

    def _load_module(self, name: str):
        self._path = name.replace(name.split('.')[-1], '')
        module = importlib.import_module(name)
        try: 
            self.modules.append(module.setup(self.bot.api, self.db).load(self.bot))
        
        except Exception as e: 
            raise NotFoundBpAttribute(e)

    def load_modules(self, folder: str = 'modules'):
        for filename in os.listdir(f'./{folder}'):
            if filename.endswith('.py'):
                try: 
                    self._load_module(f"{folder}.{filename[:-3]}")
                
                except Exception as e: 
                    raise NotFoundBpAttribute(e)



    async def run_polling(self):
        await self.bot.run_polling()




    