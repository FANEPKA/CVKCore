import json
from CVKCore.errors import AppWidgetErrors


class Tiles:

    def __init__(self, title: str, more: str, title_url: str = None, title_counter: int = None, more_url: str = None):
        self.title = title
        self.title_url = title_url
        self.more = more
        self.more_url = more_url
        self.title_counter = title_counter
        self.tiles = []

    def __repr__(self):
        try:
            code = json.dumps(self.widget, ensure_ascii=False)
            code = code.encode('utf-8').decode('utf-8')
            return f"return {code};"
        except Exception as e: raise AppWidgetErrors.WidgetError(e)

    def save(self):
        self.widget = {

            "title": self.title,
            "title_url": self.title_url,
            "title_counter": self.title_counter,
            "tiles": self.tiles,
            "more": self.more,
            "more_url": self.more_url

        }

    def add_tile(self, title: str, descr: str, icon_id: str, url: str = None, link: str = None, link_url = None):
        self.tiles.append(
            {

                "title": title,
                "descr": descr,
                "url": url,
                "link": link,
                "link_url": link_url,
                "icon_id": icon_id

            }
        )

class Table:

    def __init__(self, title: str, more: str, title_url: str = None, more_url: str = None, title_counter: int = None):
        self.title = title
        self.title_url = title_url
        self.more = more
        self.more_url = more_url
        self.title_counter = title_counter
        self.head = []
        self.body = [[]]

    def __repr__(self):
        try:
            code = json.dumps(self.widget, ensure_ascii=False)
            code = code.encode('utf-8').decode('utf-8')
            return f"return {code};"
        except Exception as e: raise AppWidgetErrors.WidgetError(e)

    def save(self):
        self.widget = {

            "title": self.title,
            "title_url": self.title_url,
            "title_counter": self.title_counter,
            "head": self.head,
            "body": self.body,
            "more": self.more,
            "more_url": self.more_url

        }

    def add_head(self, title: str, align: str):
        self.head.append(
            {

                "title": title,
                "align": align

            }
        )

    def add_body(self, text: str, icon_id: str, url: str = None):
        last_body = self.body[-1]
        last_body.append(
            {

                "text": text,
                "url": url,
                "icon_id": icon_id

            }
        )