import time
from typing import TYPE_CHECKING, Union
from datetime import datetime
from .DataBaseConnect import MySQL
from vkbottle.api import API

if TYPE_CHECKING:
    from .chat import Chat


class UserGet:
    
    @staticmethod
    def getid(member):
        if type(member) is int: return member, False
        if 'vk.com/' in member: return str(member).split('vk.com/')[1], False            
        elif '[id' in member: return str(member).split('|')[0].replace('[', ''), False
        elif '[club' in member: return str(member).split('|')[0].replace('[club', ''), True
        elif 'club' in member: return str(member).replace('club', ''), True
        else: return member, False


class User:

    def __init__(self, domain: Union[str, int], api: API, db: MySQL, name_case: str = None, fields: str = '', chat = None):
        self._id, self.is_bot = UserGet.getid(domain)
        self._api = api
        self._db = db
        self.fields = fields
        self.name_case = name_case
        self.chat: Chat = chat

    def __repr__(self):
        return f'< {__class__.__name__} id = {self.id} bot = {self.is_bot} first_name = {self.first_name} last_name = {self.last_name} >'

    async def in_chat(self):
        result = [i for i in (await self._api.messages.get_conversation_members(
                    peer_id = self.chat.peer_id, 
                    group_id=(await self._api.groups.get_by_id())[0].id
                    )
                    ).items if self.id == i.member_id]
        if result != []: return result[0]
        return False

    async def select(self):
        return await self._db.request(f"SELECT * FROM members WHERE mid = {self.id} AND pid = {self.chat.peer_id}")

    async def insert_to_db(self):
        if await self.select(): 
            return False
        
        else:
            return bool(await self._db.request(f"INSERT INTO members(mid, pid, last_message) VALUES({self.id}, {self.chat.peer_id}, {time.time()})"))

    async def intUpdate(self, event: str, arg: int):
        await self._db.request(f"UPDATE members SET {event} = {arg} WHERE mid = {self.id} AND pid = {self.chat.peer_id}")

    async def strUpdate(self, event: str, arg: str):
        await self._db.request(f"UPDATE members SET {event} = '{arg}' WHERE mid = {self.id} AND pid = {self.chat.peer_id}")

    @property
    async def is_admin(self):
        in_chat = (await self.in_chat())
        if not in_chat: 
            return None
        
        else:
            return in_chat.is_admin

    @property
    def admin(self):
        return self.db['admin'] if self.db else 0

    @property
    def display_name(self):
        if self.nick: 
            return f"{self.nick}"
        
        else:
            return self.mention

    async def change_name_case(self, name_case: str):
        if self.is_bot: 
            return False
        
        _user = (await self._api.users.get(user_ids=[self.id], name_case=name_case))[0]
        self.first_name = _user.first_name
        self.last_name = _user.last_name
        return True

    @property
    def mention(self):
        if self.is_bot: 
            return f"[club{-1*self.id}|{self.name}]"
        
        else:
            return f"[id{self.id}|{self.first_name} {self.last_name}]"

    def tag(self, name: str = 'администратором'):
        if not self.nick: 
            return ""
        
        else:
            return f"{name} "


    async def get(self):
        try:
            if not self.is_bot:
                _sex = {"female": "она", "male": "он"}
                user = (await self._api.users.get(user_ids = [str(self._id)], fields = f"sex, {self.fields}", name_case=self.name_case))[0]
                self.id, self.first_name, self.last_name = user.id, user.first_name, user.last_name
                self.options = user
                self.is_bot = False
                self.sex = _sex[self.options.sex.name]
                if await self.in_chat(): await self.insert_to_db()
                self.db = await self.select()
                if self.db: self.nick = self.db['nick'] or self.mention
                #self.created_at: datetime.datetime = await GetCreatedAt.dtime_created(self.id)
                return self
            
            else: raise Exception
        
        except:
            self.domain = str(self._id)
            if self.domain.startswith('-'): self.domain = -1*int(self.domain)
            group = (await self._api.groups.get_by_id(group_id = str(self.domain)))[0]
            if group.screen_name == str(self.domain) or str(group.id) in str(self.domain):
                self.id, self.first_name, self.last_name = -group.id, group.name, None
                self.options = group
                self.is_bot = True
                self.sex = 'она'
                self.name = self.first_name
                if await self.in_chat(): await self.insert_to_db()
                self.db = await self.select()
                if self.db: self.nick = self.db['nick'] or self.mention
                self.reg_time = None
                return self
            
            else: return False

    @staticmethod
    async def create(member_id: int, db: MySQL, api: API, chat):
        await db.request(f"INSERT INTO members(pid, mid, last_message) VALUES({chat.peer_id}, {member_id}, {time.time()})")
        return await User(member_id, api, db, chat=chat).get()