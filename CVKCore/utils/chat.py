from .DataBaseConnect import MySQL
from .user import User
from vkbottle.api import API
from vkbottle_types.responses.messages import GetConversationMembersResponse


class Chat:


    def __init__(self, peer_id: int, db: MySQL, api: API):
        self.peer_id = peer_id
        self.db = db
        self.api = api

    def __repr__(self) -> str:
        return f"< {__class__.__name__} database id = {self.id} peer_id = {self.peer_id} title = {self.title} >"


    @property
    async def owner(self) -> User:
        return await self.fetch_member(self._data['owner_id'])

    @property
    async def members(self) -> GetConversationMembersResponse:
        try: 
            return (await self.api.messages.get_conversation_members(peer_id = self.peer_id, group_id = (await self.api.groups.get_by_id())[0].id)).items
        
        except: 
            return None

    @property
    async def settings(self):
        self._settings = await self.db.request(f"SELECT * FROM settings WHERE pid = {self.peer_id}")
        return self._settings

    @property
    async def options(self):
        try: 
            return (await self.api.messages.get_conversations_by_id(peer_ids = [self.peer_id], group_id = (await self.api.groups.get_by_id())[0].id)).items[0].chat_settings
        
        except: 
            return None

    async def get_command_lvl(self, command: str):
        data = await self.db.request(f"SELECT * FROM lvl_commands WHERE command = '{command}' AND pid = {self.peer_id}")
        if data: 
            return data['lvl']
        
        else:
            return (await self.db.request(f"SELECT * FROM commands WHERE name = '{command}'"))['lvl']

    @property
    def title(self):
        return self._data['title']

    @property
    def id(self):
        return self._data['id']

    async def fetch_member(self, member_id: int):
        return await User(member_id, self.api, self.db, chat=self).get()

    async def get(self):
        self._data = await self.db.request(f"SELECT * FROM chats WHERE pid = {self.peer_id}")
        
        if self._data: 
            """
            self.prefix = self._data['prefix'].split(',') if self._data['prefix'] else ['/']
            self.local_id = self._data['lid']
            self.owner_local = bool(self._data['owner_local'])
            self.amode = bool(self._data['amode'])
            self.greeting = self._data['greeting']
            self.filter = self._data['filter'].split(',') if self._data['filter'] else None
            """            
            return self
        
        return None