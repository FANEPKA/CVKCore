import aiomysql
import pymysql
from asyncio.events import AbstractEventLoop
from aiomysql.pool import Pool


class MySQLError(Exception):
    """
    
    For errors
    
    """

class MySQL:

    def __init__(self, host: str, user: str, password: str, db: str, loop: AbstractEventLoop):
        self.host = host
        self.user = user
        self.password = password
        self.db = db
        self._data = None
        self.loop = loop

    async def _connection(self):
        pool = await aiomysql.create_pool(host = self.host, user = self.user, password = self.password, db = self.db, loop=self.loop, autocommit = True)
        return pool

    async def request(self, request: str, attr: str = 'fetchone'):
        pool: Pool = await self._connection()
        try:
            async with pool.acquire() as conn:
                async with conn.cursor(aiomysql.DictCursor) as cur:
                    try:
                    
                        await cur.execute(request)
                        self._data = cur.__getattribute__(attr)
                    
                    except AttributeError: raise AttributeError(f"Attribute \"{attr}\" not found")
                    except pymysql.err.ProgrammingError as e: raise MySQLError(e)
        finally:
            pool.close()
            await pool.wait_closed()
            return await self._data()