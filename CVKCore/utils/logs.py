import time
from .DataBaseConnect import MySQL

class Logs:

    def __init__(self, db: MySQL):
        self.db = db

    async def new_log(self, chat: int, event: str, data: str):
        await self.db.request(f"INSERT INTO botHistory(chat, event, data, time) VALUES({chat}, '{event}', '{data}', {time.time()})")