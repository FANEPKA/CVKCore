﻿## :white_check_mark: CVKCore - удобное ядро для разработки ВК ботов на Python

> Ядро работает на библиотеке [vkbottle 3.0](https://github.com/vkbottle/vkbottle)

____
## Пример кода

```python

    import asyncio
    import os
    import logging
    from CVKCore import BotConnect

    TOKENS = [] #Список токенов

    async def main():

        logging.basicConfig(level = logging.ERROR)
        loop = asyncio.get_event_loop()
        bot = BotConnect(TOKENS, loop)
        bot.load_modules(folder = 'modules')
        await bot.run_polling()
        
    asyncio.run(main())

```
